package distributore;

import java.util.Scanner;

public class Cliente {
	double credito;
	Scanner s=new Scanner(System.in);
	public Cliente() {
		credito=10.0;
	}
	public void setCredito(Double credito) {
		this.credito-=credito;
	}
	public Double getCredito() {
		return credito;
	}
	public int scelta() {
		int ritorno=2;
		System.out.println("inserire soldi nella chiavetta o coprare una bibita?");
		System.out.println("scrivere 'soldi' oppure 'compra'");
		String scelta=s.nextLine();
		if(scelta.equalsIgnoreCase("soldi")) {
			ritorno=0;
		}
		if(scelta.equalsIgnoreCase("compra")) {
			ritorno=1;
		}
		return ritorno;
	}
	public void caricaChiavetta() {
		
		System.out.println("inserire i soldi che vuoi mettere  nella chiavetta");
		double temp=s.nextDouble();
		credito+=temp;
		System.out.println("credito attuale:"+getCredito());		
	}
	
}
