package distributore;

import java.util.ArrayList;
import java.util.Scanner;

public class DistributoreImpl {
	int n = 10;
	ArrayList<Bibite>[] al = new ArrayList[n];
	private String user;
	private String password = "tavernello";
	Cliente cliente = new Cliente();
	Scanner s = new Scanner(System.in);

	public DistributoreImpl() {
		int i, j;
		for (i = 0; i < n; i = i + 1) {
			al[i] = new ArrayList<Bibite>();
			for (j = 0; j < n; j++) {
				Bibite bibita = new Bibite();
				if (i == 0) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "coca";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 1) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "coca";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 2) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "fanta";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 3) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "fanta";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 4) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "the";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 5) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "the";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 6) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "acqua";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 7) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "acqua";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 8) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "tavernello";
					bibita.timeMilli=bibita.date.getTime();
				}
				if (i == 9) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					bibita.tipo = "tavernello";
					bibita.timeMilli=bibita.date.getTime();
				}
				al[i].add(bibita);
			}
		}
	}

	public int check() {
		int b = 2;
		String tipoUtente;

		System.out.println("che utente sei? tecnico o cliente?");
		tipoUtente = s.nextLine();

		if (tipoUtente.equals("cliente")) {
			b = 0;
		} else if (tipoUtente.equals("tecnico")) {
			b = 1;
		}

		return b;
	}

	public int domanda() {
		int ritorno = 2;
		System.out.println("inserire soldi nella chiavetta o coprare una bibita?");
		System.out.println("scrivere 'soldi' oppure 'compra'");
		String scelta = s.nextLine();
		if (scelta.equalsIgnoreCase("soldi")) {
			ritorno = 1;
		}
		if (scelta.equalsIgnoreCase("compra")) {
			ritorno = 0;
		}
		if (ritorno == 2)
			domanda();
		return ritorno;
	}

	public boolean richiediPassword() {
		boolean b = false;

		System.out.println("Inserisci la password:");
		String input = s.nextLine();
		if (input.equals(password)) {
			b = true;
		}
		return b;
	}

	public void seleziona() {
		System.out.println("Seleziona bibita");
		System.out.println("Digita 0 o 1 per comprare una coca cola, costo 1,50�");
		System.out.println("Digita 2 o 3 per comprare un the, costo 1,30�");
		System.out.println("Digita 4 o 5 per comprare dell' acqua, costo 1,00�");
		System.out.println("Digita 6 o 7 per comprare una fanta, costo 1,40�");
		System.out.println("Digita 8 o 9 per comprare il tavernello, costo 3,50�");
		int digita = s.nextInt();
		Bibite bibita = new Bibite();
		bibita.tipo = null;
		if (digita == 0 || digita == 1) {
			if (cliente.credito > 1.50) {
				cliente.setCredito(1.50);
				if (al[digita].get(9).tipo != null) {
					al[digita].remove(9);
					al[digita].add(0, bibita);
					System.out.println("Hai selezionato: coca cola");
				} else {
					System.out.println("bevanda non disponibile");
				}
			} else {
				System.out.println("credito insufficente");
			}
		}
		if (digita == 2 || digita == 3) {
			if (cliente.credito > 1.30) {
				cliente.setCredito(1.30);
				if (al[digita].get(9).tipo != null) {
					al[digita].remove(9);
					al[digita].add(0, bibita);
					System.out.println("Hai selezionato: the");
				} else {
					System.out.println("bevanda non disponibile");
				}
			} else {
				System.out.println("credito insufficente");
			}
		}
		if (digita == 4 || digita == 5) {
			if (cliente.credito > 1.00) {
				cliente.setCredito(1.00);
				if (al[digita].get(9).tipo != null) {
					al[digita].remove(9);
					al[digita].add(0, bibita);
					System.out.println("Hai selezionato: acqua");
				} else {
					System.out.println("bevanda non disponibile");
				}
			} else {
				System.out.println("credito insufficente");
			}
		}
		if (digita == 6 || digita == 7) {
			if (cliente.credito > 1.40) {
				cliente.setCredito(1.40);
				if (al[digita].get(9).tipo != null) {
					al[digita].remove(9);
					al[digita].add(0, bibita);
					System.out.println("Hai selezionato: fanta");
				} else {
					System.out.println("bevanda non disponibile");
				}
			} else {
				System.out.println("credito insufficente");
			}
		}
		if (digita == 8 || digita == 9) {
			if (cliente.credito > 3.50) {
				cliente.setCredito(3.50);
				if (al[digita].get(9).tipo != null) {
					al[digita].remove(9);
					al[digita].add(0, bibita);
					System.out.println("Hai selezionato: tavernello");
				} else {
					System.out.println("bevanda non disponibile");
				}
			} else {
				System.out.println("credito insufficente");
			}
		}

		double creditoResiduo = cliente.getCredito();
		System.out.println("il tuo credito residuo �:" + creditoResiduo);
	}

	public void print() {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (al[j].get(i).tipo == null) {
					System.out.print("vuoto" + " ");
				} else {
					System.out.print(al[j].get(i).tipo + "  ");
				}
			}
			System.out.println("");
		}
	}

	public int[] quantoVuoto() {
		int[] vuoto = new int[10];
		for (int i = 0; i < n; i = i + 1) {
			for (int j = 0; j < n; j++) {
				Bibite b = new Bibite();
				b = al[i].get(j);
				if (b.tipo == null)
					vuoto[i] += 1;

			}
		}
		for (int i = 0; i < n; i = i + 1) {
			System.out.println(vuoto[i]);
		}
		return vuoto;
	}

	public void rifornisci() {
		int j;
		int[] v = quantoVuoto();
		for (int i = 0; i < 10; i++) {
			int x = v[i];
			if (x > 0) {
				for (j = 0; j <= x; j++) {

					al[i].remove(j);
					if (i == 0) {
						Bibite bibita = new Bibite();
						bibita.tipo = "coca";
						al[i].add(j, bibita);
					}
					if (i == 1) {
						Bibite bibita = new Bibite();
						bibita.tipo = "coca";
						al[i].add(j, bibita);
					}
					if (i == 2) {
						Bibite bibita = new Bibite();
						bibita.tipo = "fanta";
						al[i].add(j, bibita);
					}
					if (i == 3) {
						Bibite bibita = new Bibite();
						bibita.tipo = "fanta";
						al[i].add(j, bibita);
					}
					if (i == 4) {
						Bibite bibita = new Bibite();
						bibita.tipo = "the";
						al[i].add(j, bibita);
					}
					if (i == 5) {
						Bibite bibita = new Bibite();
						bibita.tipo = "the";
						al[i].add(j, bibita);
					}
					if (i == 6) {
						Bibite bibita = new Bibite();
						bibita.tipo = "acqua";
						al[i].add(j, bibita);
					}
					if (i == 7) {
						Bibite bibita = new Bibite();
						bibita.tipo = "acqua";
						al[i].add(j, bibita);
					}
					if (i == 8) {
						Bibite bibita = new Bibite();
						bibita.tipo = "tavernello";
						al[i].add(j, bibita);
					}
					if (i == 9) {
						Bibite bibita = new Bibite();
						bibita.tipo = "tavernello";
						al[i].add(j, bibita);
					}
				}
			}
		}
	}

	public void exit() {
		System.out.println("fine programma");
		System.exit(0);
	}

	public boolean richiestaPsd() {
		boolean a = false;
		while (richiediPassword()) {
			a = true;
			System.out.println("caricamento macchinette in corso....");
			rifornisci();
			print();
			long[] v=prossimaScadenza();
			for(int i=0;i<n;i++) {
			System.out.println("millisecondi:"+v[i]+" alla posizione:"+i);
			}
			System.out.println("vuoi uscire o ritornare alla schermata iniziale?");
			System.out.println("scrivere 'uscire' o 'inizio'");
			String temp = s.nextLine();
			if (temp.equalsIgnoreCase("uscire"))
				exit();
			if (temp.equalsIgnoreCase("inizio"))
				controllo();
		}
		return a;
	}

	
	public long[] prossimaScadenza() {
		long v[]=new long[10];
		long max;
		for (int i = 0; i < n; i++) {
			max=al[i].get(0).timeMilli;
			for (int j = 0; j < n; j++) {
				if(al[i].get(j).tipo!=null) {
				if(max >= al[i].get(j).timeMilli) {
					max=al[i].get(j).timeMilli;
				}
				}
			}
			v[i]=max;
			}
		return v;
	}
	public int controllo() {
		System.out.println("inizio");
		int verifica = check();
		
		if (verifica == 1) {
			while (!richiestaPsd())
				richiestaPsd();
		}
		if (verifica == 0) {
			if (domanda() == 0) {
				seleziona();
				print();
				controllo();
			} else {
				
				cliente.caricaChiavetta();
				controllo();
			}
		}
		if (verifica == 2) {
			controllo();
		}
		
		return verifica;
	}
	
	
}
