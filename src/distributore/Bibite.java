package distributore;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Bibite {
	Double prezzo;
	int contatore;
	int codice;
	String tipo;
	Date date;
	long timeMilli;
	
	public Bibite() {
		date = new Date();
		//System.out.println(dateFormat.format(date));
	}
	
	public Double getPrezzo() {
		return prezzo;
	}
	public int getContatore() {
		return contatore;
	}
	public int getCodice() {
		return codice;
	}
	public void getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		System.out.println(dateFormat.format(date));
	}
}
